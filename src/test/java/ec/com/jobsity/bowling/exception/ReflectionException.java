package ec.com.jobsity.bowling.exception;

/**
 * This class describe exceptions about the use of the reflection
 * @author ego
 *
 */
public class ReflectionException extends Exception {
    
	public ReflectionException(String message) {
        super(message);
    }

    public ReflectionException(Throwable throwable) {
        super(throwable);
    }
}
