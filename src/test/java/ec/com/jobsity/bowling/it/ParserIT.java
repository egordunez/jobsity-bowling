package ec.com.jobsity.bowling.it;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ec.com.jobsity.bowling.exception.InvalidMoveException;
import ec.com.jobsity.bowling.exception.ReflectionException;
import ec.com.jobsity.bowling.game.BowlingJudge;
import ec.com.jobsity.bowling.game.IJudge;
import ec.com.jobsity.bowling.game.Move;
import ec.com.jobsity.bowling.input.BowlingFileInput;
import ec.com.jobsity.bowling.input.IBowlingInput;
import ec.com.jobsity.bowling.util.Parser;
import ec.com.jobsity.bowling.util.Reflection;

public class ParserIT {

	private IBowlingInput iBowlingInput;
	private Parser parser;
	private IJudge judge;
	private Map<String, List<Move>> moveByPlayer;
	
	@BeforeEach
	public void arrange() {
		parser = new Parser();
		judge = new BowlingJudge();
	}
	
	@Test
	@DisplayName("Should count the 10 frames")
	public void framesCount() throws ReflectionException {
		iBowlingInput = new BowlingFileInput("./src/test/java/ec/com/jobsity/bowling/it/file/annotator_it_score.txt");
		moveByPlayer = parser.groupByPlayer(iBowlingInput.obtainMoves());
		judge.validate(moveByPlayer.get("Jeff"));
		assertThat(Reflection.getFieldValue(judge, "frameCount")).
		isEqualTo(10);
	}
	
	@Test
	@DisplayName("Should validate that the player can not throw the third")
	public void canNotThrowTheThird() {
		iBowlingInput = new BowlingFileInput("./src/test/java/ec/com/jobsity/bowling/it/file/annotator_it_score_frame_10.txt");
		moveByPlayer = parser.groupByPlayer(iBowlingInput.obtainMoves());
		InvalidMoveException exception = assertThrows(InvalidMoveException.class, () ->
		judge.validate(moveByPlayer.get("Jeff")));
		assertThat(exception.getMessage()).
		isEqualTo(InvalidMoveException.WRONG_TURN);
	}
	
	@Test
	@DisplayName("Should validate that the player can not throw three times")
	public void canNotThrowThreeTimes() {
		iBowlingInput = new BowlingFileInput("./src/test/java/ec/com/jobsity/bowling/it/file/annotator_it_score_3_times.txt");
		moveByPlayer = parser.groupByPlayer(iBowlingInput.obtainMoves());
		InvalidMoveException exception = assertThrows(InvalidMoveException.class, () ->
		judge.validate(moveByPlayer.get("Jeff")));
		assertThat(exception.getMessage()).
		isEqualTo(InvalidMoveException.WRONG_TURN);
	}
}
