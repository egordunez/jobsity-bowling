package ec.com.jobsity.bowling.it;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ec.com.jobsity.bowling.exception.ReflectionException;
import ec.com.jobsity.bowling.game.Annotator;
import ec.com.jobsity.bowling.game.BowlingJudge;
import ec.com.jobsity.bowling.game.IJudge;
import ec.com.jobsity.bowling.game.Player;
import ec.com.jobsity.bowling.input.BowlingFileInput;
import ec.com.jobsity.bowling.input.IBowlingInput;
import ec.com.jobsity.bowling.util.Parser;
import ec.com.jobsity.bowling.util.Reflection;
import ec.com.jobsity.bowling.util.StandartOutputItem;

public class BowlingGameIT {

	private IBowlingInput iBowlingInput;
	private IJudge judge;
	private Parser parser;
	private Annotator annotator;
	
	private final String pinFallsJeff = "Pinfalls\t\tX\t7\t/\t9\t0\t\tX\t0\t8\t8\t/\tF\t6\t\tX\t\tX\tX\t8\t1\t";
	private final String scoreJeff = "Score\t\t20\t\t39\t\t48\t\t66\t\t74\t\t84\t\t90\t\t120\t\t148\t\t167\t\t";
	
	private final String pinFallsJohn = "Pinfalls\t3\t/\t6\t3\t\tX\t8\t1\t\tX\t\tX\t9\t0\t7\t/\t4\t4\tX\t9\t0\t";
	private final String scoreJohn = "Score\t\t16\t\t25\t\t44\t\t53\t\t82\t\t101\t\t110\t\t124\t\t132\t\t151\t\t";
	
	@BeforeEach
	public void arrange() {
		judge = new BowlingJudge();
		parser = new Parser();
		iBowlingInput = new BowlingFileInput("bowling-game-it.txt");
		annotator = new Annotator(parser, iBowlingInput, judge);
	}
	
	@Test
	@DisplayName("Shuld load the file input and transform the data")
	public void fullFlux() {
		annotator.loadGames().getGameOutputList().forEach(g -> {
			try {
				Player player = (Player) Reflection.getFieldValue(g, "player");
				if (player.getName().equals("Jeff")) {
					g.print();
					assertThat(Reflection.getFieldValue(g, "gameToString").toString())
							.isEqualTo(player.getName() +
									StandartOutputItem.JUMP.value() +
									pinFallsJeff + StandartOutputItem.JUMP.value() +
									scoreJeff);
				} else if (player.getName().equals("John")) {
					g.print();
					assertThat(Reflection.getFieldValue(g, "gameToString").toString())
					.isEqualTo(player.getName() +
							StandartOutputItem.JUMP.value() +
							pinFallsJohn +
							StandartOutputItem.JUMP.value() +
							scoreJohn);
				}
			} catch (ReflectionException | IOException e) {
				e.printStackTrace();
			}
		});
	}
}
