package ec.com.jobsity.bowling.it;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ec.com.jobsity.bowling.game.Move;
import ec.com.jobsity.bowling.input.BowlingFileInput;
import ec.com.jobsity.bowling.input.IBowlingInput;

public class FileInputIT {

	private IBowlingInput iBowlingInput;
	
	@BeforeEach
	public void arrange() {
		iBowlingInput = new BowlingFileInput("fail_score_it.txt");
	}
	
	@Test
	@DisplayName("Should load the file input and transform the data")
	public void readFile() {
		List<Move> list = iBowlingInput.obtainMoves();
		for (int i = 0; i < list.size(); i++) {
			if(i < list.size() -1) {
				assertThat(list.get(i).getPlayerName()).
				isEqualTo("Carl");
				assertThat(list.get(i).getPinFalls()).
				isEqualTo(10);
			}else {
				assertThat(list.get(i).getPlayerName()).
				isEqualTo("Carl");
				assertThat(list.get(i).getPinFalls()).
				isEqualTo(-1);
			}
		}
	}
}
