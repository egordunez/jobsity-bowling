package ec.com.jobsity.bowling.frame;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class SpareFrameTest {

	private SpareFrame spareFrame;
	
	@BeforeEach
	public void arrange() {
		StrikeFrame prev = new StrikeFrame();
		StrikeFrame next = new StrikeFrame();
		spareFrame = new SpareFrame(2,8);
		spareFrame.setPrevFrame(prev);
		spareFrame.setNextFrame(next);
		prev.setNextFrame(spareFrame);
	}
	
	@Test
	@DisplayName("Should sum the previous value to it points and the next first throw")
	public void obtainPoints() {
		assertThat(spareFrame.obtainPoints()).
		isEqualTo(40);
	}
}
