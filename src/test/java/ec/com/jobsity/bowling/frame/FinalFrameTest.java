package ec.com.jobsity.bowling.frame;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ec.com.jobsity.bowling.exception.InvalidFrameException;


public class FinalFrameTest {

	private FinalFrame finalFrame;
	
	@BeforeEach
	public void arrange() {
		finalFrame = new FinalFrame(8, 2, 9);
		StrikeFrame prev = new StrikeFrame();
		prev.setNextFrame(finalFrame);
		finalFrame.setPrevFrame(prev);
	}
	
	@Test
	@DisplayName("Should return a sum of the previous value and each of throw points")
	public void obtainPoints() {
		 assertThat(finalFrame.obtainPoints()).
		 isEqualTo(39);
	}
	
	@Test
	@DisplayName("Should return the points")
	public void getFirstTrowPoints() {
		 assertThat(finalFrame.getFirstThrowPoints()).
		 isEqualTo(8);
		 assertThat(finalFrame.getSecondThrowPoints()).
		 isEqualTo(2);
		 assertThat(finalFrame.getThirdThrowPoints()).
		 isEqualTo(9);
	}
	
	@Test
	@DisplayName("Should validate in the constructor that the points at the frame never are more than 10")
	public void constructor() {
		Exception exception = assertThrows(InvalidFrameException.class, () ->
		new FinalFrame(7, 3, 11));
		assertThat(exception.getMessage()).
		isEqualTo(InvalidFrameException.UNSUPPORTED_NUMBER_OF_THROWS);
		exception = assertThrows(InvalidFrameException.class, () ->
		new FinalFrame(7, 4, 10));
		assertThat(exception.getMessage()).
		isEqualTo(InvalidFrameException.UNSUPPORTED_NUMBER_OF_THROWS);
	}
	
	@Test
	@DisplayName("Should validate in the constructor that the points at the frame never are less than 0")
	public void constructorLessThanZero() {
		Exception exception = assertThrows(InvalidFrameException.class, () ->
		new FinalFrame(-1, 3, 10));
		assertThat(exception.getMessage()).
		isEqualTo(InvalidFrameException.UNSUPPORTED_PINFALLS);
		
		exception = assertThrows(InvalidFrameException.class, () ->
		new FinalFrame(7, -1, 10));
		assertThat(exception.getMessage()).
		isEqualTo(InvalidFrameException.UNSUPPORTED_PINFALLS);
		
		exception = assertThrows(InvalidFrameException.class, () ->
		new FinalFrame(7, 3, -1));
		assertThat(exception.getMessage()).
		isEqualTo(InvalidFrameException.UNSUPPORTED_PINFALLS);
	}
	
	@Test
	@DisplayName("Should validate in the constructor when the final frame can do the third throw")
	public void constructorThrowCount() {
		Exception exception = assertThrows(InvalidFrameException.class, () ->
		new FinalFrame(3, 3, 3));
		assertThat(exception.getMessage()).
		isEqualTo(InvalidFrameException.UNSUPPORTED_NUMBER_OF_THROWS);
	}
}
