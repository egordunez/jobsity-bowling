package ec.com.jobsity.bowling.frame;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class StrikeFrameTest {

	private StrikeFrame strikeFrame;
	
	@BeforeEach
	public void arrange() {
		Frame prev = new Frame(2, 6);
		strikeFrame = new StrikeFrame();
		strikeFrame.setPrevFrame(prev);
		Frame next = new Frame(5, 3);
		strikeFrame.setNextFrame(next);
	}
	
	@Test
	@DisplayName("Sum it points to the next 2 first throw at frame")
	public void obtainPoints(){
		assertThat(strikeFrame.obtainPoints()).
		isEqualTo(26);
	}
	
	@Test
	@DisplayName("Should return the points of first and second throw at frame")
	public void getFirstTrowPoints(){
		assertThat(strikeFrame.getFirstThrowPoints()).
		isEqualTo(10);
		assertThat(strikeFrame.getSecondThrowPoints()).
		isEqualTo(0);
	}
}
