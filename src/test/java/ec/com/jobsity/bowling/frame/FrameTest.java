package ec.com.jobsity.bowling.frame;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ec.com.jobsity.bowling.exception.InvalidFrameException;

public class FrameTest {

	private Frame frame;
	
	@BeforeEach
	public void arrange() {
		StrikeFrame prev = new StrikeFrame();
		frame = new Frame(6,3);
		prev.setNextFrame(frame);
		frame.setPrevFrame(prev);
	}
	
	@Test
	@DisplayName("Should sum the previous point received to it total points")
	public void obtainPoints() {
		assertThat(frame.obtainPoints()).
		isEqualTo(28);
	}
	
	@Test
	@DisplayName("Should validate in constructor that the points at the frame never are more than 10")
	public void constructor() {
		Exception exception = assertThrows(InvalidFrameException.class, () ->
		new Frame(7,3));
		assertThat(exception.getMessage()).
		isEqualTo(InvalidFrameException.UNSUPPORTED_NUMBER_OF_THROWS);
	}
	
	@Test
	@DisplayName("Should validate in constructor that the points are never less than -1")
	public void contructorLessThanZero() {
		Exception exception = assertThrows(InvalidFrameException.class, () ->
		new Frame(-2,4));
		assertThat(exception.getMessage()).
		isEqualTo(InvalidFrameException.UNSUPPORTED_PINFALLS);
	}
}
