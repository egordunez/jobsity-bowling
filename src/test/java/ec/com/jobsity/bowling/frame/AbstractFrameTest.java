package ec.com.jobsity.bowling.frame;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ec.com.jobsity.bowling.exception.InvalidFrameException;

public class AbstractFrameTest {

	private AbstractFrame abstractFrame;
	
	@BeforeEach
	public void arrange() {
		abstractFrame = new SpareFrame(7,3);
	}
	
	@Test
	@DisplayName("Should return the points of it first and second throw")
	public void getFirstTrowPoints() {
		assertThat(abstractFrame.getFirstThrowPoints()).
		isEqualTo(7);
		assertThat(abstractFrame.getSecondThrowPoints()).
		isEqualTo(3);
	}
	
	@Test
	@DisplayName("Should validate a constructor that the sum of points are never more than 10")
	public void contructor() {
		Exception exception = assertThrows(InvalidFrameException.class, () ->
		new SpareFrame(7,4));
		assertThat(exception.getMessage()).
		isEqualTo(InvalidFrameException.UNSUPPORTED_NUMBER_OF_THROWS);
	}
	
	@Test
	@DisplayName("Should validate a constructor that the points are never less than 0")
	public void contructorLessThanZero() {
		Exception exception = assertThrows(InvalidFrameException.class, () ->
		new SpareFrame(-1,4));
		assertThat(exception.getMessage()).
		isEqualTo(InvalidFrameException.UNSUPPORTED_PINFALLS);
	}
}
