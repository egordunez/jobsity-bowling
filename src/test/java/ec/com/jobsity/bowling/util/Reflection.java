package ec.com.jobsity.bowling.util;

import java.lang.reflect.Field;

import ec.com.jobsity.bowling.exception.ReflectionException;

/**
 * Used to access to fields by reflection
 * @author ego
 *
 */
public final class Reflection {

    public static final String EXCEPTION_MESSAGE = "No such field: ";

    private Reflection() {
        throw new IllegalStateException("Utility class");
    }

    private static Field getField(Class<?> aClass, String fieldName) throws ReflectionException {
        if (aClass == null) {
            throw new ReflectionException(EXCEPTION_MESSAGE + fieldName);
        }

        try {
            return aClass.getDeclaredField(fieldName);
        } catch (NoSuchFieldException e) {
            return getField(aClass.getSuperclass(), fieldName);
        }
    }

    public static Object getFieldValue(Object affectedObject, String fieldName) throws ReflectionException {
        if (affectedObject == null || fieldName == null) {
            throw new ReflectionException(EXCEPTION_MESSAGE + fieldName);
        }

        Field affectedField = getField(affectedObject.getClass(), fieldName);
        affectedField.setAccessible(true);

        try {
            return affectedField.get(affectedObject);
        } catch (IllegalAccessException e) {
            throw new ReflectionException(e);
        }
    }

    public static void setFieldValue(Object affectedObject, String fieldName, Object newValue) throws ReflectionException {
        if (affectedObject == null || fieldName == null) {
            throw new ReflectionException(EXCEPTION_MESSAGE + fieldName);
        }

        Field affectedField = getField(affectedObject.getClass(), fieldName);
        affectedField.setAccessible(true);

        try {
            affectedField.set(affectedObject, newValue);
        } catch (IllegalAccessException e) {
            throw new ReflectionException(e);
        }
    }

}
