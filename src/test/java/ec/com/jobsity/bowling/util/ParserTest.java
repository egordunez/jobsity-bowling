package ec.com.jobsity.bowling.util;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ec.com.jobsity.bowling.game.Move;

public class ParserTest {

	private Parser parser;
	private List<Move> plays;
	
	@BeforeEach
	public void arrange() {
		parser = new Parser();
	}
	
	@Test
	@DisplayName("Should validate when the players count is up to 2")
	public void groupByPlayerWith3() {
		plays = new ArrayList<>();
		plays.add(new Move("A", 10));
		plays.add(new Move("B", 10));
		plays.add(new Move("C", 10));
		plays.add(new Move("C", 10));
		plays.add(new Move("A", 10));
		plays.add(new Move("B", 10));
	}
	
	@Test
	@DisplayName("Should group players by it player name")
	public void groupByPlayer() {
		plays = new ArrayList<>();
		plays.add(new Move("A", 7));
		plays.add(new Move("A", 3));
		plays.add(new Move("B", 10));
		plays.add(new Move("A", 10));
		plays.add(new Move("B", 2));
		plays.add(new Move("B", 8));
		Map<String, List<Move>> moveByPlayer = parser.groupByPlayer(plays);
		assertThat(moveByPlayer.get("A").stream().mapToInt(i -> i.getPinFalls()).sum()).
		isEqualTo(20);
		assertThat(moveByPlayer.get("B").stream().mapToInt(i -> i.getPinFalls()).sum()).
		isEqualTo(20);
		assertThat(moveByPlayer.get("A").stream().filter(m -> m.getPlayerName().equals("A")).count()).
		isEqualTo(3);
		assertThat(moveByPlayer.get("B").stream().filter(m -> m.getPlayerName().equals("B")).count()).
		isEqualTo(3);
		
	}
	
}
