package ec.com.jobsity.bowling.game;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import ec.com.jobsity.bowling.exception.ReflectionException;
import ec.com.jobsity.bowling.frame.IFrame;
import ec.com.jobsity.bowling.input.IBowlingInput;
import ec.com.jobsity.bowling.output.IFrameOutput;
import ec.com.jobsity.bowling.output.IGameOutput;
import ec.com.jobsity.bowling.output.frame.FinalFrameOutput;
import ec.com.jobsity.bowling.output.frame.FrameOutput;
import ec.com.jobsity.bowling.output.frame.SpareFrameOutput;
import ec.com.jobsity.bowling.output.frame.StrikeFrameOutput;
import ec.com.jobsity.bowling.util.Parser;
import ec.com.jobsity.bowling.util.Reflection;

public class AnnotatorTest {

	private Annotator annotator;
	@Mock
	private Parser parser;
	@Mock
	private IBowlingInput iBowlingInput;
	@Mock
	private IJudge judge;
	private List<Move> moves;
	
	@BeforeEach
	public void arrange() {
		judge = new BowlingJudge();
		MockitoAnnotations.initMocks(this);
		Map<String, List<Move>> fakeMoveByPlayer = new HashMap<>();
		moves = new ArrayList<>();
		moves.add(new Move("Jeff", 10, 0));//0
		moves.add(new Move("Jeff", 7, 3));//1
		moves.add(new Move("Jeff", 3, 4));//1
		moves.add(new Move("Jeff", 9, 7));//2
		moves.add(new Move("Jeff", 0, 8));//2
		moves.add(new Move("Jeff", 10, 10));//3
		moves.add(new Move("Jeff", 0, 13));//4
		moves.add(new Move("Jeff", 8, 14));//4
		moves.add(new Move("Jeff", 8, 16));//5
		moves.add(new Move("Jeff", 2, 17));//5
		moves.add(new Move("Jeff", -1, 19));//6
		moves.add(new Move("Jeff", 6, 20));//6
		moves.add(new Move("Jeff", 10, 23));//7
		moves.add(new Move("Jeff", 10, 26));//8
		moves.add(new Move("Jeff", 10, 29));//9
		moves.add(new Move("Jeff", 8, 30));//9
		moves.add(new Move("Jeff", 1, 31));//9
		fakeMoveByPlayer.put("Jeff", moves);
		Mockito.when(parser.groupByPlayer(Mockito.anyList())).
		thenReturn(fakeMoveByPlayer);
		annotator =  new Annotator(parser, iBowlingInput,judge);
	}
	
	@Test
	@DisplayName("Should call at least once time the methods for validate")
	public void loadGames() throws ReflectionException {
		Map<String, List<Move>> fakeMoveByPlayer = new HashMap<>();
		fakeMoveByPlayer.put("Jeff", moves);
		fakeMoveByPlayer.put("Carl", moves);
		Mockito.when(parser.groupByPlayer(Mockito.anyList())).
		thenReturn(fakeMoveByPlayer);
		annotator =  new Annotator(parser, iBowlingInput, judge);
		assertThat(annotator.loadGames()).
		isInstanceOf(Annotator.class);
		Mockito.verify(judge, Mockito.atLeastOnce()).validate(Mockito.anyList());
	}
	
	@Test
	@DisplayName("Should create at least once game")
	public void createGame() throws ReflectionException {
		annotator.loadGames();
		List<IGameOutput> gameOutputList = annotator.getGameOutputList();
		assertThat(gameOutputList.size()).
		isEqualTo(1);
		List<IFrameOutput> frameOutputList = (List<IFrameOutput>) Reflection.getFieldValue(gameOutputList.get(0), "frameOutputList");
		assertThat(frameOutputList.stream().filter(i -> i instanceof StrikeFrameOutput).count()).
		isEqualTo(4);
		assertThat(frameOutputList.stream().filter(i -> i instanceof SpareFrameOutput).count()).
		isEqualTo(2);
		assertThat(frameOutputList.stream().filter(i -> i instanceof FrameOutput).count()).
		isEqualTo(3);
		assertThat(frameOutputList.stream().filter(i -> i instanceof FinalFrameOutput).count()).
		isEqualTo(1);
		
		IFrame frame = (IFrame) Reflection.getFieldValue(frameOutputList.get(0), "frame");
		assertThat(frame.getOneThrowPoints()).
		isEqualTo(10);
		assertThat(frame.getTwoThrowPoints()).
		isEqualTo(17);
		
		frame = (IFrame) Reflection.getFieldValue(frameOutputList.get(1), "frame");
		assertThat(frame.getOneThrowPoints()).
		isEqualTo(7);
		assertThat(frame.getTwoThrowPoints()).
		isEqualTo(10);
		
		frame = (IFrame) Reflection.getFieldValue(frameOutputList.get(2), "frame");
		assertThat(frame.getOneThrowPoints()).
		isEqualTo(9);
		assertThat(frame.getTwoThrowPoints()).
		isEqualTo(9);
		
		frame = (IFrame) Reflection.getFieldValue(frameOutputList.get(3), "frame");
		assertThat(frame.getOneThrowPoints()).
		isEqualTo(10);
		assertThat(frame.getTwoThrowPoints()).
		isEqualTo(10);
		
		frame = (IFrame) Reflection.getFieldValue(frameOutputList.get(4), "frame");
		assertThat(frame.getOneThrowPoints()).
		isEqualTo(0);
		assertThat(frame.getTwoThrowPoints()).
		isEqualTo(8);
		
		frame = (IFrame) Reflection.getFieldValue(frameOutputList.get(5), "frame");
		assertThat(frame.getOneThrowPoints()).
		isEqualTo(8);
		assertThat(frame.getTwoThrowPoints()).
		isEqualTo(10);
		
		frame = (IFrame) Reflection.getFieldValue(frameOutputList.get(6), "frame");
		assertThat(frame.getOneThrowPoints()).
		isEqualTo(0);
		assertThat(frame.getTwoThrowPoints()).
		isEqualTo(6);
		
		frame = (IFrame) Reflection.getFieldValue(frameOutputList.get(7), "frame");
		assertThat(frame.getOneThrowPoints()).
		isEqualTo(10);
		assertThat(frame.getTwoThrowPoints()).
		isEqualTo(20);
		
		frame = (IFrame) Reflection.getFieldValue(frameOutputList.get(8), "frame");
		assertThat(frame.getOneThrowPoints()).
		isEqualTo(10);
		assertThat(frame.getTwoThrowPoints()).
		isEqualTo(20);
		
		frame = (IFrame) Reflection.getFieldValue(frameOutputList.get(9), "frame");
		assertThat(frame.getOneThrowPoints()).
		isEqualTo(10);
		assertThat(frame.getTwoThrowPoints()).
		isEqualTo(18);
	}
}
