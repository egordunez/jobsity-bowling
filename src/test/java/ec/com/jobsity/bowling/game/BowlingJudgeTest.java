package ec.com.jobsity.bowling.game;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ec.com.jobsity.bowling.exception.GameException;
import ec.com.jobsity.bowling.exception.InvalidMoveException;

public class BowlingJudgeTest {

	private IJudge judge;
	private List<Move> plays;
	
	@BeforeEach
	public void arrange() {
		judge = new BowlingJudge();
	}
	
	@Test
	@DisplayName("Should validate the order of moves")
	public void validatePlayerMoves() {
		plays = new ArrayList<>();
		plays.add(new Move("A", 10, 0));
		plays.add(new Move("A", 7, 1));
		plays.add(new Move("A", 3, 4));
		plays.add(new Move("A", 9, 7));
		plays.add(new Move("A", 0, 8));
		plays.add(new Move("A", 10, 10));
		Exception exception = assertThrows(InvalidMoveException.class, () ->
		judge.validate(plays));
		assertThat(exception.getMessage()).
		isEqualTo(InvalidMoveException.WRONG_TURN);
		
		plays = new ArrayList<>();
		plays.add(new Move("A", 10, 0));
		plays.add(new Move("A", 7, 3));
		plays.add(new Move("A", 3, 4));
		plays.add(new Move("A", 9, 7));
		plays.add(new Move("A", 0, 10));
		plays.add(new Move("A", 10, 10));
		exception = assertThrows(InvalidMoveException.class, () ->
		judge.validate(plays));
		assertThat(exception.getMessage()).
		isEqualTo(InvalidMoveException.WRONG_TURN);
	}

	@Test
	@DisplayName("Should validate when exist unallowed number of throws")
	public void validatePlayerNumberMoves() {
		plays = new ArrayList<>();
		plays.add(new Move("A", 10, 0));//0
		
		plays.add(new Move("A", 7, 3));//1
		plays.add(new Move("A", 3, 4));//2
		
		plays.add(new Move("A", 9, 7));//3
		plays.add(new Move("A", 0, 8));//4
		
		plays.add(new Move("A", 10, 10));//5
		
		plays.add(new Move("A", 0, 13));//6
		plays.add(new Move("A", 8, 14));//7
		
		plays.add(new Move("A", 8, 16));//8
		plays.add(new Move("A", 2, 17));//9
		
		plays.add(new Move("A", -1, 19));//10
		plays.add(new Move("A", 6, 20));//11
		
		plays.add(new Move("A", 10, 23));//12
		
		plays.add(new Move("A", 10, 26));//13
		
		plays.add(new Move("A", 10, 29));//14
		plays.add(new Move("A", 8, 30));//15
		plays.add(new Move("A", 1, 31));//16
		
		plays.add(new Move("A", 10, 23));//17
		Exception exception = assertThrows(GameException.class, () ->
		judge.validate(plays));
		assertThat(exception.getMessage()).
		isEqualTo(GameException.FULL_GAME);
	}
}
