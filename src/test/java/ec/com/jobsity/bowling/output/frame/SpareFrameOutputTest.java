package ec.com.jobsity.bowling.output.frame;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ec.com.jobsity.bowling.frame.IFrame;
import ec.com.jobsity.bowling.frame.SpareFrame;
import ec.com.jobsity.bowling.output.IFrameOutput;

public class SpareFrameOutputTest {

	private IFrameOutput spareFrameOutput;
	private IFrame spareFrame1;
	private IFrame spareFrame2;
	
	@BeforeEach
	public void arrange() {
		spareFrame1 = new SpareFrame(8, 2);
		spareFrame2 = new SpareFrame(7, 3);
		spareFrame1.setNextFrame(spareFrame2);
		spareFrameOutput = new SpareFrameOutput();
		spareFrameOutput.setFrame(spareFrame1);
	}
	
	@Test
	@DisplayName("Should return the right string for this frame object")
	public void toStringTest() {
		assertThat(spareFrameOutput.pinFallsToString()).
		isEqualTo("8\t/");
		assertThat(spareFrameOutput.scoreToString()).
		isEqualTo("17");
	}
}
