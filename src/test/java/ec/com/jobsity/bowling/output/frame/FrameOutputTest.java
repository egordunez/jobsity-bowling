package ec.com.jobsity.bowling.output.frame;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ec.com.jobsity.bowling.frame.Frame;
import ec.com.jobsity.bowling.frame.IFrame;
import ec.com.jobsity.bowling.frame.SpareFrame;
import ec.com.jobsity.bowling.output.IFrameOutput;

public class FrameOutputTest {

	private IFrameOutput frameOutput;
	private IFrame frame;
	private IFrame spareFrame;
	
	@BeforeEach
	public void arrange() {
		frame = new Frame(7, 2);
		spareFrame = new SpareFrame(7, 3);
		frame.setNextFrame(spareFrame);
		frameOutput = new FrameOutput();
		frameOutput.setFrame(frame);
	}
	
	@Test
	@DisplayName("Should return the right string for this frame object")
	public void toStringTest() {
		assertThat(frameOutput.pinFallsToString()).
		isEqualTo("7\t2");
		assertThat(frameOutput.scoreToString()).
		isEqualTo("9");
	}
}
