package ec.com.jobsity.bowling.output.game;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ec.com.jobsity.bowling.exception.GameException;
import ec.com.jobsity.bowling.exception.ReflectionException;
import ec.com.jobsity.bowling.frame.FinalFrame;
import ec.com.jobsity.bowling.frame.Frame;
import ec.com.jobsity.bowling.frame.SpareFrame;
import ec.com.jobsity.bowling.frame.StrikeFrame;
import ec.com.jobsity.bowling.game.Player;
import ec.com.jobsity.bowling.output.IGameOutput;
import ec.com.jobsity.bowling.output.frame.FinalFrameOutput;
import ec.com.jobsity.bowling.output.frame.FrameOutput;
import ec.com.jobsity.bowling.output.frame.SpareFrameOutput;
import ec.com.jobsity.bowling.output.frame.StrikeFrameOutput;
import ec.com.jobsity.bowling.util.Reflection;
import ec.com.jobsity.bowling.util.StandartOutputItem;

public class GameStandardOutputTest {
	
	private final String pinFalls = "Pinfalls\t8\t/\t7\t/\t3\t4\t\tX\t2\t/\t\tX\t\tX\t8\tF\t\tX\t8\t/\t9\t";
	private final String score = "Score\t\t17\t\t30\t\t37\t\t57\t\t77\t\t105\t\t123\t\t131\t\t151\t\t170\t\t";

	private IGameOutput gameStandardOutput;
	private Player player;
	private SpareFrame f1;
	private SpareFrameOutput fo1;
	private SpareFrame f2;
	private SpareFrameOutput fo2;
	private Frame f3;
	private FrameOutput fo3;
	private StrikeFrame f4;
	private StrikeFrameOutput fo4;
	private SpareFrame f5;
	private SpareFrameOutput fo5;
	private StrikeFrame f6;
	private StrikeFrameOutput fo6;
	private StrikeFrame f7;
	private StrikeFrameOutput fo7;
	private Frame f8;
	private FrameOutput fo8;
	private StrikeFrame f9;
	private StrikeFrameOutput fo9;
	private FinalFrame ff;
	private FinalFrameOutput ffo;
	
	@BeforeEach
	public void arrange() {
		player = new Player("Neto");
		gameStandardOutput = new GameStandardOutput(player, System.out);
		
		f1 = new SpareFrame(8, 2);
		fo1 = new SpareFrameOutput();
		fo1.setFrame(f1);
		
		f2 = new SpareFrame(7, 3);
		fo2 = new SpareFrameOutput();
		fo2.setFrame(f2);
		
		f3 = new Frame(3, 4);
		fo3 = new FrameOutput();
		fo3.setFrame(f3);
		
		f4 = new StrikeFrame();
		fo4 = new StrikeFrameOutput();
		fo4.setFrame(f4);
		
		f5 = new SpareFrame(2, 8);
		fo5 = new SpareFrameOutput();
		fo5.setFrame(f5);
		
		f6 = new StrikeFrame();
		fo6 = new StrikeFrameOutput();
		fo6.setFrame(f6);
		
		f7 = new StrikeFrame();
		fo7 = new StrikeFrameOutput();
		fo7.setFrame(f7);
		
		f8 = new Frame(8, -1);
		fo8 = new FrameOutput();
		fo8.setFrame(f8);
		
		f9 = new StrikeFrame();
		fo9 = new StrikeFrameOutput();
		fo9.setFrame(f9);
		
		ffo = new FinalFrameOutput();
		
		gameStandardOutput.addFrameOutput(fo1);
		gameStandardOutput.addFrameOutput(fo2);
		gameStandardOutput.addFrameOutput(fo3);
		gameStandardOutput.addFrameOutput(fo4);
		gameStandardOutput.addFrameOutput(fo5);
		gameStandardOutput.addFrameOutput(fo6);
		gameStandardOutput.addFrameOutput(fo7);
		gameStandardOutput.addFrameOutput(fo8);
		gameStandardOutput.addFrameOutput(fo9);
	}
	
	@Test
	@DisplayName("Should validate when a game have 10 frames")
	public void addFrame() {
		ff = new FinalFrame(10, 10, 10);
		ffo.setFrame(ff);
		gameStandardOutput.addFrameOutput(ffo);
		Exception exception = assertThrows(GameException.class, () ->
		gameStandardOutput.addFrameOutput(new SpareFrameOutput()));
		assertThat(exception.getMessage()).
		isEqualTo(GameException.FULL_GAME);
	}
	
	@Test
	@DisplayName("Should validate that the last frame is FinalFrame")
	public void addFinalFrame() {
		Exception exception = assertThrows(GameException.class, () ->
		gameStandardOutput.addFrameOutput(new StrikeFrameOutput()));
		assertThat(exception.getMessage()).
		isEqualTo(GameException.FINAL_FRAME_ERROR);
	}
	
	@Test
	@DisplayName("Should obtain the points of the game")
	public void obtainPoints() throws ReflectionException, IOException {
		ff = new FinalFrame(8, 2, 9);
		ffo.setFrame(ff);
		gameStandardOutput.addFrameOutput(ffo);
		prepareStrikeFrameWith2Next();
		gameStandardOutput.print();
		assertThat(Reflection.getFieldValue(gameStandardOutput, "gameToString").toString()).
		isEqualTo(player.getName() +
				StandartOutputItem.JUMP.value() +
				pinFalls +
				StandartOutputItem.JUMP.value() +
				score +
				StandartOutputItem.JUMP.value());
	}
	
	private void prepareStrikeFrameWith2Next() {
		f1.setNextFrame(f2);
		f2.setPrevFrame(f1);
		
		f2.setNextFrame(f3);
		f3.setPrevFrame(f2);
		
		f3.setNextFrame(f4);
		f4.setPrevFrame(f3);
		
		f4.setNextFrame(f5);
		f5.setPrevFrame(f4);
		
		f5.setNextFrame(f6);
		f6.setPrevFrame(f5);
		
		f6.setNextFrame(f7);
		f7.setPrevFrame(f6);
		
		f7.setNextFrame(f8);
		f8.setPrevFrame(f7);
		
		f8.setNextFrame(f9);
		f9.setPrevFrame(f8);
		
		f9.setNextFrame(ff);
		ff.setPrevFrame(f9);
	}
	
	@AfterEach
	public void clean() {
		gameStandardOutput = null;
		player = null;
		f1 = null;
		f2 = null;
		f3 = null;
		f4 = null;
		f5 = null;
		f6 = null;
		f7 = null;
		f8 = null;
		f9 = null;
		ff = null;
	}
}
