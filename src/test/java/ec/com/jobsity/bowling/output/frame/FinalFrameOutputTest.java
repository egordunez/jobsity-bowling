package ec.com.jobsity.bowling.output.frame;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ec.com.jobsity.bowling.frame.FinalFrame;
import ec.com.jobsity.bowling.frame.IFrame;
import ec.com.jobsity.bowling.frame.SpareFrame;
import ec.com.jobsity.bowling.output.IFrameOutput;

public class FinalFrameOutputTest {

	private IFrameOutput finalFrameOutput;
	private IFrame finalFrame;
	private IFrame spareFrame;
	
	@BeforeEach
	public void arrange() {
		finalFrameOutput = new FinalFrameOutput();
	}
	
	@Test
	@DisplayName("Should return the right string for this frame object")
	public void toStringTest() {
		finalFrame = new FinalFrame(10, 10, 10);
		spareFrame = new SpareFrame(7, 3);
		finalFrame.setPrevFrame(spareFrame);
		spareFrame.setNextFrame(finalFrame);
		finalFrameOutput.setFrame(finalFrame);
		assertThat(finalFrameOutput.pinFallsToString()).
		isEqualTo("X\tX\tX");
		assertThat(finalFrameOutput.scoreToString()).
		isEqualTo("50");
		
		finalFrame = new FinalFrame(7, 3, 10);
		spareFrame = new SpareFrame(7, 3);
		finalFrame.setPrevFrame(spareFrame);
		spareFrame.setNextFrame(finalFrame);
		finalFrameOutput.setFrame(finalFrame);
		assertThat(finalFrameOutput.pinFallsToString()).
		isEqualTo("7\t/\tX");
		assertThat(finalFrameOutput.scoreToString()).
		isEqualTo("37");
		
		finalFrame = new FinalFrame(7, 2, 0);
		spareFrame = new SpareFrame(7, 3);
		finalFrame.setPrevFrame(spareFrame);
		spareFrame.setNextFrame(finalFrame);
		finalFrameOutput.setFrame(finalFrame);
		assertThat(finalFrameOutput.pinFallsToString()).
		isEqualTo("7\t2\t0");
		assertThat(finalFrameOutput.scoreToString()).
		isEqualTo("26");
	}
}
