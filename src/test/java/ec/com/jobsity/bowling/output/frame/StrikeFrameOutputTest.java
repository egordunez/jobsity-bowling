package ec.com.jobsity.bowling.output.frame;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ec.com.jobsity.bowling.frame.IFrame;
import ec.com.jobsity.bowling.frame.SpareFrame;
import ec.com.jobsity.bowling.frame.StrikeFrame;
import ec.com.jobsity.bowling.output.IFrameOutput;

public class StrikeFrameOutputTest {

	private IFrameOutput strikeFrameOutput;
	private IFrame strikeFrame;
	private IFrame spareFrame;
	
	@BeforeEach
	public void arrange() {
		strikeFrame = new StrikeFrame();
		spareFrame = new SpareFrame(7, 3);
		strikeFrame.setNextFrame(spareFrame);
		strikeFrameOutput = new StrikeFrameOutput();
		strikeFrameOutput.setFrame(strikeFrame);
	}
	
	@Test
	@DisplayName("Should return the right string for this frame object")
	public void toStringTest() {
		assertThat(strikeFrameOutput.pinFallsToString()).
		isEqualTo("\tX");
		assertThat(strikeFrameOutput.scoreToString()).
		isEqualTo("20");
	}
}
