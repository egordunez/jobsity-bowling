package ec.com.jobsity.bowling.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ec.com.jobsity.bowling.game.Move;

/**
 * Transform an validate the list of plays to Game
 * @author ego
 *
 */
public class Parser {

	public Map<String, List<Move>> groupByPlayer(List<Move> plays) {
		Map<String, List<Move>> moveByPlayer = new HashMap<>();
		for (Move move : plays) {
			List<Move> moveList = moveByPlayer.get(move.getPlayerName());
			if(moveList == null) {
				moveList = new ArrayList<>();
			}
			moveList.add(move);
			moveByPlayer.put(move.getPlayerName(), moveList);
		}
		return moveByPlayer;
	}
	
}
