package ec.com.jobsity.bowling.util;

public enum StandartOutputItem {

	TAB("\t"),
	JUMP("\n"),
	FOUL("F"),
	STRIKE("X"),
	SPARE_CLOSE("/"),
	FRAME_HEADER("Frame\t\t1\t\t2\t\t3\t\t4\t\t5\t\t6\t\t7\t\t8\t\t9\t\t10"),
	PINFALLS("Pinfalls"),
	SCORE("Score");
	
	
	private final String value;

	StandartOutputItem(final String newValue) {
        value = newValue;
    }

    public String value() { return value; }
}
