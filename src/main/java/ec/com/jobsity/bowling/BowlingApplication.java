package ec.com.jobsity.bowling;

import java.io.IOException;
import java.nio.file.NoSuchFileException;

import ec.com.jobsity.bowling.exception.GameException;
import ec.com.jobsity.bowling.exception.InvalidMoveException;
import ec.com.jobsity.bowling.game.Annotator;
import ec.com.jobsity.bowling.game.BowlingJudge;
import ec.com.jobsity.bowling.game.IJudge;
import ec.com.jobsity.bowling.input.BowlingFileInput;
import ec.com.jobsity.bowling.input.IBowlingInput;
import ec.com.jobsity.bowling.util.Parser;
import ec.com.jobsity.bowling.util.StandartOutputItem;

public class BowlingApplication {

	public static void run(String[] args) throws NoSuchFileException{
		String fileName = null;
		if(args.length > 0) {
			fileName = args[0];
		}
		IBowlingInput iBowlingInput = new BowlingFileInput(fileName == null ? "bowling-game.txt" : fileName);
		Parser parser = new Parser();
		IJudge judge = new BowlingJudge();
		Annotator annotator = new Annotator(parser, iBowlingInput, judge);
		System.out.println(StandartOutputItem.FRAME_HEADER.value());
		annotator.loadGames().getGameOutputList().forEach(g -> {
			try {
				g.print();
			} catch (IOException e) {
				System.out.println("Filed to read file");
			}
		});
	}
	
	public static void main(String[] args) {
		try {
			run(args);
		} catch (InvalidMoveException e) {
			System.out.println(e.getMessage());
		} catch (GameException e) {
			System.out.println(e.getMessage());
		} catch (NoSuchFileException e) {
			System.out.println("File not found");
		} catch (Exception e) {
			System.out.println("Some error has happened");
		}
	}
}

