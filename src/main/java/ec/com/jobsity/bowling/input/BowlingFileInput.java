package ec.com.jobsity.bowling.input;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import ec.com.jobsity.bowling.exception.InvalidMoveException;
import ec.com.jobsity.bowling.frame.FramePoint;
import ec.com.jobsity.bowling.game.Move;
import ec.com.jobsity.bowling.util.StandartOutputItem;

/**
 * Concretion of an input by file
 * @author ego
 *
 */
public class BowlingFileInput implements IBowlingInput{

	private String fileName;

	public BowlingFileInput(String fileName) {
		super();
		this.fileName = fileName;
	}

	@Override
	public List<Move> obtainMoves() {
		final List<Move> fromFile = new ArrayList<>();
		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
			int [] index = new int[] {0};
			stream.forEach((String line) -> {
				String [] splitted = line.split(StandartOutputItem.TAB.value());
				Move newMove = new Move(splitted[0],
						splitted[1].equals(StandartOutputItem.FOUL.value()) ?
								FramePoint.FOUL.value() : Integer.parseInt(splitted[1]));
				newMove.setInputIndex(index[0]);
				fromFile.add(newMove);
				index[0]++;
			});
		} catch (NumberFormatException e) {
			throw new InvalidMoveException(InvalidMoveException.UNSUPORTED_MOVE);
		}catch (IOException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return fromFile;
	}
}
