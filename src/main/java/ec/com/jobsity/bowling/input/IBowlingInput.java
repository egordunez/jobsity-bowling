package ec.com.jobsity.bowling.input;

import java.util.List;

import ec.com.jobsity.bowling.game.Move;

/**
 * Abstraction of the input of plays made by a player
 * @author ego
 *
 */
public interface IBowlingInput {

	public List<Move> obtainMoves();
}
