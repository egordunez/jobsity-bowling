package ec.com.jobsity.bowling.frame;

public enum FramePoint {

	STRIKE(10),
	FOUL(-1),
	MIN_POINTS(0);
	
	private final int value;

	FramePoint(final int newValue) {
        value = newValue;
    }

    public int value() { return value; }
}
