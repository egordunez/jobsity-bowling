package ec.com.jobsity.bowling.frame;

import ec.com.jobsity.bowling.exception.InvalidFrameException;

/**
 * The concretion of a strike frame
 * @author ego
 *
 */
public class StrikeFrame implements IFrame {

	private int points;
	private IFrame nextFrame;
	private IFrame prevFrame;
	
	public StrikeFrame() {
		this.points = FramePoint.STRIKE.value();
		validate();
	}
	
	@Override
	public int obtainPoints() {
		int prevPoints = prevFrame == null ? FramePoint.MIN_POINTS.value() : prevFrame.obtainPoints();
		int nextPoints = nextFrame.getTwoThrowPoints();
		int points = this.points + prevPoints + nextPoints;
		return points;
	}

	@Override
	public int getOneThrowPoints() {
		return this.points;
	}

	@Override
	public int getTwoThrowPoints() {
		return this.points + nextFrame.getOneThrowPoints();
	}

	@Override
	public int getFirstThrowPoints() {
		return this.points;
	}

	@Override
	public int getSecondThrowPoints() {
		return FramePoint.MIN_POINTS.value();
	}

	@Override
	public void setNextFrame(IFrame nextThrow) {
		this.nextFrame = nextThrow;
	}

	@Override
	public void setPrevFrame(IFrame prevThrow) {
		this.prevFrame = prevThrow;
	}

	@Override
	public int getThirdThrowPoints() {
		return FramePoint.MIN_POINTS.value();
	}

	@Override
	public void validate() {
		if (this.points < FramePoint.MIN_POINTS.value() ||
				this.points > FramePoint.STRIKE.value()) {
			throw new InvalidFrameException(InvalidFrameException.UNSUPPORTED_PINFALLS);
		}
	}

}
