package ec.com.jobsity.bowling.frame;

import ec.com.jobsity.bowling.exception.InvalidFrameException;

/**
 * The concretion of a frame that is Spare
 * @author ego
 *
 */
public class SpareFrame extends AbstractFrame {

	public SpareFrame(int firstThrowPoints, int secondThrowPoints) {
		this.firstThrowPoints = firstThrowPoints;
		this.secondThrowPoints = secondThrowPoints;
		validate();
	} 
	
	@Override
	public int obtainPoints() {
		return obtainPreviousPoints() +
				firstThrowPoints +
				secondThrowPoints +
				nextFrame.getOneThrowPoints();
	}

	@Override
	public void validate() {
		if (firstThrowPoints + secondThrowPoints > FramePoint.STRIKE.value()) {
			throw new InvalidFrameException(InvalidFrameException.UNSUPPORTED_NUMBER_OF_THROWS);
		}
		if (firstThrowPoints < FramePoint.MIN_POINTS.value() || secondThrowPoints < FramePoint.MIN_POINTS.value()) {
			throw new InvalidFrameException(InvalidFrameException.UNSUPPORTED_PINFALLS);
		}
	}

}
