package ec.com.jobsity.bowling.frame;

import ec.com.jobsity.bowling.exception.InvalidFrameException;

/**
 * The concretion of the frame that was not Strike or Spare
 * @author ego
 *
 */
public class Frame extends AbstractFrame {

	public Frame(int firstThrowPoints, int secondThrowPoints) {
		this.firstThrowPoints = firstThrowPoints;
		this.secondThrowPoints = secondThrowPoints;
		validate();
	}
	
	@Override
	public int obtainPoints() {
		return obtainPreviousPoints() +
				(firstThrowPoints == FramePoint.FOUL.value() ?
						FramePoint.MIN_POINTS.value() : firstThrowPoints) +
				(secondThrowPoints == FramePoint.FOUL.value() ?
						FramePoint.MIN_POINTS.value() : secondThrowPoints);
	}
	
	@Override
	public void validate() {
		if (firstThrowPoints + secondThrowPoints > FramePoint.STRIKE.value() ||
				firstThrowPoints + secondThrowPoints >= FramePoint.STRIKE.value()) {
			throw new InvalidFrameException(InvalidFrameException.UNSUPPORTED_NUMBER_OF_THROWS);
		} else if (firstThrowPoints < FramePoint.FOUL.value() || secondThrowPoints < FramePoint.FOUL.value()) {
			throw new InvalidFrameException(InvalidFrameException.UNSUPPORTED_PINFALLS);
		}
	}

}
