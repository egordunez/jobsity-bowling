package ec.com.jobsity.bowling.frame;

/**
 * An abstraction of a frame that can contain 2 throws
 * @author ego
 *
 */
public abstract class AbstractFrame implements IFrame {

	protected int firstThrowPoints;
	protected int secondThrowPoints;
	protected IFrame prevFrame;
	protected IFrame nextFrame;
	
	protected int obtainPreviousPoints() {
		return prevFrame == null ? FramePoint.MIN_POINTS.value() : prevFrame.obtainPoints();
	}
	
	@Override
	public int getFirstThrowPoints() {
		return this.firstThrowPoints;
	}

	@Override
	public int getSecondThrowPoints() {
		return this.secondThrowPoints;
	}

	@Override
	public int getOneThrowPoints() {
		return this.firstThrowPoints == FramePoint.FOUL.value() ? FramePoint.MIN_POINTS.value() : this.firstThrowPoints;
	}
	
	@Override
	public int getTwoThrowPoints() {
		return (this.firstThrowPoints == FramePoint.FOUL.value() ? FramePoint.MIN_POINTS.value() : this.firstThrowPoints) +
				(this.secondThrowPoints == FramePoint.FOUL.value() ? FramePoint.MIN_POINTS.value() : this.secondThrowPoints);
	}

	@Override
	public void setPrevFrame(IFrame prevFrame) {
		this.prevFrame = prevFrame;
	}

	@Override
	public void setNextFrame(IFrame nextFrame) {
		this.nextFrame = nextFrame;
	}

	@Override
	public int getThirdThrowPoints() {
		return FramePoint.MIN_POINTS.value();
	}
	
}
