package ec.com.jobsity.bowling.frame;

import ec.com.jobsity.bowling.exception.InvalidFrameException;

/**
 * A concretion of the final frame in a game
 * @author ego
 *
 */
public class FinalFrame extends AbstractFrame {

	private int thirdThrowPoints;
	
	public FinalFrame(int firstThrowPoints, int secondThrowPoints, int thirdThrowPoints) {
		this.firstThrowPoints = firstThrowPoints;
		this.secondThrowPoints = secondThrowPoints;
		this.thirdThrowPoints = thirdThrowPoints;
		validate();
	}
	
	@Override
	public int obtainPoints() {
		return obtainPreviousPoints() +
				firstThrowPoints +
				secondThrowPoints +
				thirdThrowPoints;
	}

	@Override
	public int getThirdThrowPoints() {
		return thirdThrowPoints;
	}

	@Override
	public void validate() {
		if (firstThrowPoints < FramePoint.MIN_POINTS.value() ||
				secondThrowPoints < FramePoint.MIN_POINTS.value() ||
				thirdThrowPoints < FramePoint.MIN_POINTS.value()) {
			throw new InvalidFrameException(InvalidFrameException.UNSUPPORTED_PINFALLS);
		} else if (firstThrowPoints < FramePoint.STRIKE.value() &&
				secondThrowPoints < FramePoint.STRIKE.value() &&
				firstThrowPoints + secondThrowPoints > FramePoint.STRIKE.value() ||
				firstThrowPoints > FramePoint.STRIKE.value() ||
				secondThrowPoints > FramePoint.STRIKE.value() ||
				thirdThrowPoints > FramePoint.STRIKE.value() ||
				(thirdThrowPoints > FramePoint.MIN_POINTS.value() &&
				firstThrowPoints + secondThrowPoints < FramePoint.STRIKE.value())) {
			throw new InvalidFrameException(InvalidFrameException.UNSUPPORTED_NUMBER_OF_THROWS);
		}
	}
}
