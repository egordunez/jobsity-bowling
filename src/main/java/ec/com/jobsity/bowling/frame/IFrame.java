package ec.com.jobsity.bowling.frame;

/**
 * The abstraction of a frame
 * @author ego
 *
 */
public interface IFrame {

	int obtainPoints();
	
	int getOneThrowPoints();
	
	int getTwoThrowPoints();
	
	int getFirstThrowPoints();
	
	int getSecondThrowPoints();
	
	int getThirdThrowPoints();
	
	void setNextFrame(IFrame nextThrow);

	void setPrevFrame(IFrame prevThrow);
	
	void validate();
	
}
