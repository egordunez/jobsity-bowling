package ec.com.jobsity.bowling.exception;

/**
 * This class describe exceptions about the creation of a Game
 * @author ego
 *
 */
public class GameException extends RuntimeException{
	
	public static final String FULL_GAME = "FULL_GAME";
	public static final String FINAL_FRAME_ERROR = "FINAL_FRAME_ERROR";
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3736622923730850194L;

	public GameException(String message) {
		super(message);
	}
}
