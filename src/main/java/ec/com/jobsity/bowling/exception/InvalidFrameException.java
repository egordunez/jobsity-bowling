package ec.com.jobsity.bowling.exception;

/**
 * This class describe exceptions about the creation of a Frame
 * @author ego
 *
 */
public class InvalidFrameException extends RuntimeException{
	
	public static final String UNSUPPORTED_NUMBER_OF_THROWS = "UNSUPPORTED_NUMBER_OF_THROWS";
	public static final String UNSUPPORTED_PINFALLS = "UNSUPPORTED_PINFALLS";
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3736622923730850194L;

	public InvalidFrameException(String message) {
		super(message);
	}
}
