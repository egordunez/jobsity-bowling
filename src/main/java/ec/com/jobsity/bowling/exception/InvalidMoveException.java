package ec.com.jobsity.bowling.exception;

/**
 * This class describe exceptions about the creation of a Move
 * @author ego
 *
 */
public class InvalidMoveException extends RuntimeException{
	
	public static final String WRONG_TURN = "WRONG_TURN";
	public static final String UNSUPORTED_MOVE = "UNSUPORTED_MOVE";
	/**
	 * 
	 */
	private static final long serialVersionUID = 3736622923730850194L;

	public InvalidMoveException(String message) {
		super(message);
	}
}
