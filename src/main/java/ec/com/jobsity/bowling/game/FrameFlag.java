package ec.com.jobsity.bowling.game;

public enum FrameFlag {

	FRAME_COUNT(10),
	FINAL_FRAME_INDEX(9);
	
	private final int value;

	FrameFlag(final int newValue) {
        value = newValue;
    }

    public int value() { return value; }
}
