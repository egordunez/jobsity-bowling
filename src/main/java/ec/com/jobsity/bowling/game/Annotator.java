package ec.com.jobsity.bowling.game;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ec.com.jobsity.bowling.frame.FinalFrame;
import ec.com.jobsity.bowling.frame.Frame;
import ec.com.jobsity.bowling.frame.FramePoint;
import ec.com.jobsity.bowling.frame.IFrame;
import ec.com.jobsity.bowling.frame.SpareFrame;
import ec.com.jobsity.bowling.frame.StrikeFrame;
import ec.com.jobsity.bowling.input.IBowlingInput;
import ec.com.jobsity.bowling.output.IFrameOutput;
import ec.com.jobsity.bowling.output.IGameOutput;
import ec.com.jobsity.bowling.output.frame.FinalFrameOutput;
import ec.com.jobsity.bowling.output.frame.FrameOutput;
import ec.com.jobsity.bowling.output.frame.SpareFrameOutput;
import ec.com.jobsity.bowling.output.frame.StrikeFrameOutput;
import ec.com.jobsity.bowling.output.game.GameStandardOutput;
import ec.com.jobsity.bowling.util.Parser;

public class Annotator {

	private IJudge judge;
	private Parser parser;
	private IBowlingInput bowlingInput;
	private List<IGameOutput> gameOutputList;

	public Annotator(Parser parser, IBowlingInput iBowlingInput, IJudge judge) {
		this.judge = judge;
		this.parser = parser;
		this.bowlingInput = iBowlingInput;
		this.gameOutputList = new ArrayList<>();
		
	}

	public Annotator loadGames() {
		Map<String, List<Move>> moveByPlayer = parser.groupByPlayer(bowlingInput.obtainMoves());
		moveByPlayer.keySet().forEach(playerName -> {
			if(moveByPlayer.keySet().size() > 1) {
				judge.validate(moveByPlayer.get(playerName));
			}
			this.gameOutputList.add(createGame(playerName, moveByPlayer.get(playerName)));
		});
		return this;
	}

	private IGameOutput createGame(String playerName, List<Move> movesByPlayer) {
		IGameOutput gameOutput = new GameStandardOutput(new Player(playerName), System.out);
		int frameCount = 0;
		//will be used to store the previous processed frame
		IFrame previousFrame = null;
		//Will be used to store the new created frame
		IFrame currentFrame = null;
		IFrameOutput currentFrameOutput = null;
		//This loop will finish when all frame of a game has been processed
		for (int i = 0; frameCount < FrameFlag.FRAME_COUNT.value();) {
			if(frameCount == FrameFlag.FINAL_FRAME_INDEX.value()) {
				currentFrame = buildFinalFrame(movesByPlayer.get(i), movesByPlayer.get(i + 1), movesByPlayer.get(i + 2));
				currentFrameOutput = new FinalFrameOutput();
			}else {
				if(movesByPlayer.get(i).getPinFalls() == FramePoint.STRIKE.value()) {
					currentFrame = buildStrikeFrame();
					currentFrameOutput = new StrikeFrameOutput();
					i++;
				}
				else if(isASpareFrame(movesByPlayer.get(i), movesByPlayer.get(i + 1))) {
					currentFrame = buildSpareFrame(movesByPlayer.get(i), movesByPlayer.get(i + 1));
					currentFrameOutput = new SpareFrameOutput();
					i+=2;
				}
				else if(isAFrame(movesByPlayer.get(i), movesByPlayer.get(i + 1))) {
					currentFrame = buildFrame(movesByPlayer.get(i), movesByPlayer.get(i + 1));
					currentFrameOutput = new FrameOutput();
					i+=2;
				}
			}
			currentFrameOutput.setFrame(currentFrame);
			gameOutput.addFrameOutput(currentFrameOutput);
			//Creating the doubly linked list
			currentFrame.setPrevFrame(previousFrame);
			if(previousFrame != null) {
				previousFrame.setNextFrame(currentFrame);
			}
			previousFrame = currentFrame;
			frameCount++;
		}
		return gameOutput;
	}

	public List<IGameOutput> getGameOutputList() {
		return gameOutputList;
	}
	
	private IFrame buildFrame(Move firstMove, Move secondMove) {
		IFrame currentFrame = null;
		currentFrame = new Frame(firstMove.getPinFalls(), secondMove.getPinFalls());
		return currentFrame;
	}
	
	private IFrame buildSpareFrame(Move firstMove, Move secondMove) {
		IFrame currentFrame = null;
		currentFrame = new SpareFrame(firstMove.getPinFalls(), secondMove.getPinFalls());
		return currentFrame;
	}
	
	private IFrame buildStrikeFrame() {
		IFrame currentFrame = null;
		currentFrame = new StrikeFrame();
		return currentFrame;
	}
	
	private IFrame buildFinalFrame(Move firstMove, Move secondMove, Move thirdMove) {
		IFrame currentFrame = null;
		if(haveThirdThrow(firstMove, secondMove)) {
			currentFrame = new FinalFrame(
					firstMove.getPinFalls(),
					secondMove.getPinFalls(),
					thirdMove.getPinFalls());
		}else {
			currentFrame = new FinalFrame(
					firstMove.getPinFalls(),
					secondMove.getPinFalls(),
					FramePoint.MIN_POINTS.value());
		}
		return currentFrame;
	}
	
	private boolean haveThirdThrow(Move move, Move adjacentMove) {
		return move.getPinFalls() == FramePoint.STRIKE.value() ||
				move.getPinFalls() +
				adjacentMove.getPinFalls() == FramePoint.STRIKE.value();
	}
	
	private boolean isAFrame(Move move, Move adjacentMove) {
		return move.getPinFalls() < FramePoint.STRIKE.value() &&
				move.getPinFalls() + adjacentMove.getPinFalls() < FramePoint.STRIKE.value();
	}
	
	private boolean isASpareFrame(Move move, Move adjacentMove) {
		return move.getPinFalls() < FramePoint.STRIKE.value() &&
				move.getPinFalls() + adjacentMove.getPinFalls() == FramePoint.STRIKE.value();
	}

}
