package ec.com.jobsity.bowling.game;

import java.util.List;

public interface IJudge {

	void validate(List<Move> moves);
}
