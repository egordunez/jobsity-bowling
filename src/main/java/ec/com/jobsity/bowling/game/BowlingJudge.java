package ec.com.jobsity.bowling.game;

import java.util.List;

import ec.com.jobsity.bowling.exception.GameException;
import ec.com.jobsity.bowling.exception.InvalidMoveException;
import ec.com.jobsity.bowling.frame.FramePoint;

public class BowlingJudge implements IJudge {

	private int frameCount;
	
	@Override
	public void validate(List<Move> movesFromPlayer) {
		frameCount = 0;
		int size = movesFromPlayer.size();
		for (int i = 0; frameCount <= FrameFlag.FINAL_FRAME_INDEX.value();) {
			if (frameCount == FrameFlag.FINAL_FRAME_INDEX.value()) {
				//If exist more move that are not allowed
				if(i + 2 < movesFromPlayer.size() - 1) {
					throw new GameException(GameException.FULL_GAME);
				} else if(// check if the first throw at the 9th frame is X or the first and second is spare
				((movesFromPlayer.get(i).getPinFalls() == FramePoint.STRIKE.value()
						|| movesFromPlayer.get(i).getPinFalls() +
						movesFromPlayer.get(i + 1).getPinFalls() == FramePoint.STRIKE.value())
								&&
								// So must exist a third throw
								size - 1 < i + 2)
				||
				// check if the first throw at the 9th frame is not X or the first and second is
				// not spare
				((movesFromPlayer.get(i).getPinFalls() < FramePoint.STRIKE.value()
						|| movesFromPlayer.get(i).getPinFalls() +
						movesFromPlayer.get(i + 1).getPinFalls() < FramePoint.STRIKE.value())
						&&
						// So must not exist a third throw
						size - 1 == i + 2)) {
					throw new InvalidMoveException(InvalidMoveException.WRONG_TURN);
				}
			}else if(
					//Check if the throw is X
					movesFromPlayer.get(i).getPinFalls() == FramePoint.STRIKE.value() &&
					//So the next index must not be up in one
					movesFromPlayer.get(i).getInputIndex() + 1 == movesFromPlayer.get(i + 1).getInputIndex() ||
					//Check if the throw is not X
					movesFromPlayer.get(i).getPinFalls() < FramePoint.STRIKE.value() &&
					//so the next index throw must be up in one
					movesFromPlayer.get(i).getInputIndex() + 1 != movesFromPlayer.get(i + 1).getInputIndex() ||
					movesFromPlayer.get(i).getInputIndex() + 2 == movesFromPlayer.get(i + 2).getInputIndex()) {
				throw new InvalidMoveException(InvalidMoveException.WRONG_TURN);
			}
			if(movesFromPlayer.get(i).getPinFalls() < FramePoint.STRIKE.value()) {
				i += 2;
			}
			else {
				i++;
			}
			frameCount++;
		}

	}

}
