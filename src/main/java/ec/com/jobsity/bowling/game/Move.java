package ec.com.jobsity.bowling.game;

import lombok.Getter;
import lombok.Setter;

/**
 * DTO used to store the plays in order made by a player 
 * @author ego
 *
 */
@Getter
@Setter
public class Move {

	private String playerName;
	private int pinFalls;
	private int inputIndex;
	
	public Move(String playerName, int pintFalls) {
		this.playerName = playerName;
		this.pinFalls = pintFalls;
	}

	public Move(String playerName, int pintFalls, int inputIndex) {
		this.playerName = playerName;
		this.pinFalls = pintFalls;
		this.inputIndex = inputIndex;
	}
	
}
