package ec.com.jobsity.bowling.game;

/**
 * Describe a player
 * @author ego
 *
 */
public class Player {

	private String name;
	
	public Player(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
