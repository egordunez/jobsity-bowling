package ec.com.jobsity.bowling.output;

import java.io.IOException;

public interface IGameOutput {

	void addFrameOutput(IFrameOutput frameOutput);

	void print() throws IOException;
}
