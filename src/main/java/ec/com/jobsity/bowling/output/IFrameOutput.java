package ec.com.jobsity.bowling.output;

import ec.com.jobsity.bowling.frame.IFrame;

public interface IFrameOutput {

	void setFrame(IFrame frame);
	
	String scoreToString();
	
	String pinFallsToString();
}
