package ec.com.jobsity.bowling.output.frame;

import ec.com.jobsity.bowling.frame.FramePoint;
import ec.com.jobsity.bowling.frame.IFrame;
import ec.com.jobsity.bowling.output.IFrameOutput;
import ec.com.jobsity.bowling.util.StandartOutputItem;

public class FinalFrameOutput implements IFrameOutput {

	private IFrame frame;
	
	@Override
	public void setFrame(IFrame frame) {
		this.frame = frame;
	}

	@Override
	public String scoreToString() {
		return frame.obtainPoints() + "";
	}

	@Override
	public String pinFallsToString() {
		StringBuilder toString = new StringBuilder();
		toString.append(frame.getFirstThrowPoints() == FramePoint.STRIKE.value() ?
				StandartOutputItem.STRIKE.value() : frame.getFirstThrowPoints());
		toString.append(StandartOutputItem.TAB.value());
		if(frame.getSecondThrowPoints() == FramePoint.STRIKE.value()) {
			toString.append(StandartOutputItem.STRIKE.value());
		} else if(frame.getFirstThrowPoints() + frame.getSecondThrowPoints() == FramePoint.STRIKE.value()) {
			toString.append(StandartOutputItem.SPARE_CLOSE.value());
		} else {
			toString.append(frame.getSecondThrowPoints());
		}
		toString.append(StandartOutputItem.TAB.value());
		toString.append(frame.getThirdThrowPoints() == FramePoint.STRIKE.value() ?
				StandartOutputItem.STRIKE.value() : frame.getThirdThrowPoints());
		return toString.toString();
	}
	
}
