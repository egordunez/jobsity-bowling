package ec.com.jobsity.bowling.output.game;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import ec.com.jobsity.bowling.exception.GameException;
import ec.com.jobsity.bowling.frame.FramePoint;
import ec.com.jobsity.bowling.game.FrameFlag;
import ec.com.jobsity.bowling.game.Player;
import ec.com.jobsity.bowling.output.IFrameOutput;
import ec.com.jobsity.bowling.output.IGameOutput;
import ec.com.jobsity.bowling.output.frame.FinalFrameOutput;
import ec.com.jobsity.bowling.util.StandartOutputItem;

public class GameStandardOutput implements IGameOutput {

	private Player player;
	private List<IFrameOutput> frameOutputList;
	private StringBuilder gameToString;
	private OutputStream outputStream;
	
	public GameStandardOutput(Player player, OutputStream outputStream) {
		this.player = player;
		this.outputStream = outputStream;
		frameOutputList = new ArrayList<>();
	}
	
	@Override
	public void addFrameOutput(IFrameOutput frameOutput) {
		if(frameOutputList.size() == FramePoint.STRIKE.value()) {
			throw new GameException(GameException.FULL_GAME);
		}else if(
				frameOutputList.size() == FrameFlag.FINAL_FRAME_INDEX.value() && !(frameOutput instanceof FinalFrameOutput) ||
				frameOutputList.size() < FrameFlag.FINAL_FRAME_INDEX.value() && frameOutput instanceof FinalFrameOutput) {
			throw new GameException(GameException.FINAL_FRAME_ERROR);
		}
		this.frameOutputList.add(frameOutput);
	}

	@Override
	public void print() throws IOException {
		StringBuilder pinFalls = new StringBuilder(StandartOutputItem.PINFALLS.value());
		pinFalls.append(StandartOutputItem.TAB.value());
		StringBuilder score = new StringBuilder(StandartOutputItem.SCORE.value());
		score.append(StandartOutputItem.TAB.value());
		score.append(StandartOutputItem.TAB.value());
		gameToString = new StringBuilder();
		frameOutputList.forEach(fo -> {
			pinFalls.append(fo.pinFallsToString());
			pinFalls.append(StandartOutputItem.TAB.value());
			score.append(fo.scoreToString());
			score.append(StandartOutputItem.TAB.value());
			score.append(StandartOutputItem.TAB.value());
		});
		gameToString.append(player.getName());
		gameToString.append(StandartOutputItem.JUMP.value());
		gameToString.append(pinFalls);
		gameToString.append(StandartOutputItem.JUMP.value());
		gameToString.append(score);
		gameToString.append(StandartOutputItem.JUMP.value());
		outputStream.write(gameToString.toString().getBytes());
	}

}
