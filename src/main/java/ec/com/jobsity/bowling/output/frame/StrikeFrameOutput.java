package ec.com.jobsity.bowling.output.frame;

import ec.com.jobsity.bowling.frame.IFrame;
import ec.com.jobsity.bowling.output.IFrameOutput;
import ec.com.jobsity.bowling.util.StandartOutputItem;

public class StrikeFrameOutput implements IFrameOutput {

	private IFrame frame;
	
	@Override
	public void setFrame(IFrame frame) {
		this.frame = frame;
	}

	@Override
	public String scoreToString() {
		return frame.obtainPoints() + "";
	}

	@Override
	public String pinFallsToString() {
		StringBuilder toString = new StringBuilder();
		toString.append(StandartOutputItem.TAB.value());
		toString.append(StandartOutputItem.STRIKE.value());
		return toString.toString();
	}
	
}
