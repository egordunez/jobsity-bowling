# bowling

Using the Makefile you can run diferents commands using the GNU/Linux terminal.

##Run the unit test
make test

##Run the integration test
make it

##Build the project
make jar

##Run the app with the optional parameter fn (with as default'bowling-game.txt') as the file name to use
make run-app fn=bowling-game.txt

##Run the report of test coverage. Will be saved at ./build/jacocoHtml
make coverage


##Notes
The project was made using Gradle.
For the unit test was used the assertj and mockito librarys.
I try to use pitest for mutation test, but I can't found the way to use it.
The project suport one or more players by file.
The integration test classes are under .it package.
The unit test classes are with the suffix *Test.


